#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QApplication, QWidget,QLabel, QGridLayout
from PyQt5.QtWidgets import QPushButton, QHBoxLayout, QLineEdit, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt

class Kalkulator(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.configure_window()
        self.setWindowTitle("Sth simple")
        self.setWindowIcon(QIcon('kalkulator.png'))
        self.figureResult.setToolTip('Wpisz <b>liczby</b> i wybierz działanie...')
        self.show()

    def configure_infos_widgets(self,shapeGrid):
        #napisy
        label_1=QLabel("First number: ",self)
        label_2=QLabel("Second number: ",self)
        label_3=QLabel("Result: ",self)
        #generacja napisów jako pól w oknie
        shapeGrid.addWidget(label_1, 0,0)
        shapeGrid.addWidget(label_2, 0,1)
        shapeGrid.addWidget(label_3, 0,2)

    def configure_val_fields(self,shapeGrid):
        #generacja pól jako ramek czy coś
        self.figure1 = QLineEdit()
        self.figure1.setFocus()
        self.figure2 = QLineEdit()
        self.figureResult = QLineEdit()
        self.figureResult.readonly = True

        #dodawanie tego to okna
        shapeGrid.addWidget(self.figure1,1,0)
        shapeGrid.addWidget(self.figure2,1,1)
        shapeGrid.addWidget(self.figureResult,1,2)

    def koniec(self):
        self.close()

    def configure_buttons(self,shapeHBox,shapeGrid):
        one  = QPushButton("1", self)
        two  = QPushButton("2", self)
        three = QPushButton("3", self)
        four = QPushButton("4", self)
        five = QPushButton("5", self)
        six = QPushButton("6", self)
        seven = QPushButton("7", self)
        eight = QPushButton("8", self)
        nine = QPushButton("9", self)
        zero = QPushButton("0", self)

        shapeHBox.addWidget(one)
        shapeHBox.addWidget(two)
        shapeHBox.addWidget(three)
        shapeHBox.addWidget(four)
        shapeHBox.addWidget(five)
        shapeHBox.addWidget(six)
        shapeHBox.addWidget(seven)
        shapeHBox.addWidget(eight)
        shapeHBox.addWidget(nine)
        shapeHBox.addWidget(zero)

        addBtn = QPushButton("Dodaj", self)
        subBtn = QPushButton("Odejmij", self)
        divBtn = QPushButton("Mnóż", self)
        mulBtn = QPushButton("Dziel", self)
        endBtn = QPushButton("Koniec", self)
        endBtn.resize(endBtn.sizeHint())
        endBtn.clicked.connect(self.koniec)
        addBtn.clicked.connect(self.operation)
        subBtn.clicked.connect(self.operation)
        mulBtn.clicked.connect(self.operation)
        divBtn.clicked.connect(self.operation)
        #dodawanie do okna
        shapeHBox.addWidget(addBtn) #TODO dlaczego tutaj nie musze ustawiac koordynatow wiersza i kolumny
        shapeHBox.addWidget(subBtn)
        shapeHBox.addWidget(mulBtn)
        shapeHBox.addWidget(divBtn)
        shapeGrid.addWidget(endBtn,4,0,1,3)

    def operation(self):
        try:
            figure1=float(self.figure1.text())
            figure2=float(self.figure2.text())
            result=''
        except ValueError:
            QMessageBox.warning(self, "Bład danych", QMessageBox.Ok)
        clicked_button=self.sender().text()
        if clicked_button == "Dodaj":
            result = figure1+figure2
            self.figureResult.setText(str(result))
        elif clicked_button == "Odejmij":
            result = figure1-figure2
            self.figureResult.setText(str(result))
        elif clicked_button == "Mnóż":
            result = figure1*figure2
            self.figureResult.setText(str(result))
        else:
            try:
                result=round(figure1/figure2,9)
                self.figureResult.setText(str(result))
            except ZeroDivisionError:
                QMessageBox.critical(self, "Błąd", "Nie można dzielić przez zero!")
                return


    def configure_window(self):
        self.resize(200,80) # wielkosc startowa okna
        shapeGrid= QGridLayout() #layout w ksztalcie siatki
        shapeHBox = QHBoxLayout() #tego nie rozumiem TODO
        shapeHBox_2 = QHBoxLayout()
        self.configure_infos_widgets(shapeGrid)
        self.configure_val_fields(shapeGrid)
        self.configure_buttons(shapeHBox,shapeGrid)

        shapeGrid.addLayout(shapeHBox,3,0,1,3)
        self.setLayout(shapeGrid)

    def pressed_detection(self, event):
        if event.key() == Qt.Key_F4:
            return True

    def keyPressEvent(self, event):
        if event.key() == (self.pressed_detection(event) and Qt.Key_Alt):
            self.close()


    def closeEvent(self, event):
        window_name="Komunikat"
        message = "Czy na pewno zamknąć"
        answer = QMessageBox.question(self, window_name, message,
            QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if answer == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    okno = Kalkulator()
    sys.exit(app.exec_())
